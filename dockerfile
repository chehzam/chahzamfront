FROM node:16 as build

WORKDIR /home/node

COPY package*.json ./

RUN npm ci --prefer-offline;

COPY . .


RUN  npm run build \  
  && rm -r /home/node/.angular;


FROM nginx:latest as run

COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY --from=build /home/node/dist /usr/share/nginx/html