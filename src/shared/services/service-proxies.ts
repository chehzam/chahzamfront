// USERS

export enum RoleName {
  Admin = 'ADMIN',
  User = 'USER',
  None = 'NONE'
}

export interface IUserDto {
  id: number;
  userName: string;
  roleName: string;
  firstName: string;
  lastName: string;
  token: string;
}

export class UserDto implements IUserDto {
  id = 0;
  userName = '';
  roleName = RoleName.None;
  token = '';
  firstName = '';
  lastName = '';

  constructor(data?: IUserDto) {
    if (data) {
      for (const property in data) {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(_data?: any) {
    if (_data) {
      this.id = _data['id_user'];
      this.userName = _data['user_name'];
      this.roleName = _data['role'];
      this.token = _data['token'];
      this.firstName = _data['firstname'];
      this.lastName = _data['lastname'];
    }
  }

  static fromJS(data: any): UserDto {
    data = typeof data === 'object' ? data : {};
    const result = new UserDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['id'] = this.id;
    data['user_name'] = this.userName;
    data['rolename'] = this.roleName;
    data['token'] = this.token;
    data['firstname'] = this.firstName;
    data['lastname'] = this.lastName;
    return data;
  }

  clone(): UserDto {
    const json = this.toJSON();
    const result = new UserDto();
    result.init(json);
    return result;
  }
}

// COVER

export interface ICoverDto {
  id_cover: number;
  image: string;
}

export class CoverDto implements ICoverDto {
  id_cover = 0;
  image = '';

  constructor(data?: ICoverDto) {
    if (data) {
      for (const property in data) {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(_data?: any) {
    if (_data) {
      this.id_cover = _data['id_cover'];
      this.image = _data['image'];
    }
  }

  static fromJS(data: any): CoverDto {
    data = typeof data === 'object' ? data : {};
    const result = new CoverDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['id_cover'] = this.id_cover;
    data['image'] = this.image;
    return data;
  }

  clone(): CoverDto {
    const json = this.toJSON();
    const result = new CoverDto();
    result.init(json);
    return result;
  }
}

// AUTHORS

export interface IAuthorDto {
  id_artist: number;
  name: string;
}

export class AuthorDto implements IAuthorDto {
  id_artist = 0;
  name = '';

  constructor(data?: IAuthorDto) {
    if (data) {
      for (const property in data) {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(_data?: any) {
    if (_data) {
      this.id_artist = _data['id_artist'];
      this.name = _data['name'];
    }
  }

  static fromJS(data: any): AuthorDto {
    data = typeof data === 'object' ? data : {};
    const result = new AuthorDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['id_artist'] = this.id_artist;
    data['name'] = this.name;
    return data;
  }

  clone(): AuthorDto {
    const json = this.toJSON();
    const result = new AuthorDto();
    result.init(json);
    return result;
  }
}

// ALBUM

export interface IAlbumDto {
  id_album: number;
  title: string;
  cover: ICoverDto;
}

export class AlbumDto implements IAlbumDto {
  id_album = 0;
  title = '';
  cover: CoverDto = new CoverDto();

  constructor(data?: IAlbumDto) {
    if (data) {
      for (const property in data) {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(_data?: any) {
    if (_data) {
      this.id_album = _data['id_album'];
      this.title = _data['title'];
      this.cover = CoverDto.fromJS(_data['cover']);
    }
  }

  static fromJS(data: any): AlbumDto {
    data = typeof data === 'object' ? data : {};
    const result = new AlbumDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['id_album'] = this.id_album;
    data['title'] = this.title;
    data['cover'] = this.cover.toJSON();
    return data;
  }

  clone(): AlbumDto {
    const json = this.toJSON();
    const result = new AlbumDto();
    result.init(json);
    return result;
  }
}

// MUSIC

export interface IMusicDto {
  id_music: number;
  title: string;
  artist: IAuthorDto;
  featuring: IAuthorDto[];
  album: IAlbumDto;
}

export class MusicDto implements IMusicDto {
  id_music = 0;
  title = '';
  artist = new AuthorDto();
  featuring: AuthorDto[] = [];
  album: AlbumDto = new AlbumDto();

  constructor(data?: IMusicDto) {
    if (data) {
      for (const property in data) {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(_data?: any) {
    if (_data) {
      this.id_music = _data['id_music'];
      this.title = _data['title'];
      this.artist = AuthorDto.fromJS(_data['artist']);
      if (Array.isArray(_data['featuring'])) {
        this.featuring = [] as any;
        for (const item of _data['featuring']) this.featuring.push(AuthorDto.fromJS(item));
      }
      this.album = AlbumDto.fromJS(_data['album']);
    }
  }

  static fromJS(data: any): MusicDto {
    data = typeof data === 'object' ? data : {};
    const result = new MusicDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['id_music'] = this.id_music;
    data['title'] = this.title;
    if (Array.isArray(this.featuring)) {
      data['featuring'] = [];
      for (const item of this.featuring) data['featuring'].push(item.toJSON());
    }
    data['album'] = this.album.toJSON();
    data['artist'] = this.artist.toJSON();
    return data;
  }

  clone(): MusicDto {
    const json = this.toJSON();
    const result = new MusicDto();
    result.init(json);
    return result;
  }
}
