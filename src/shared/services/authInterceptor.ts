import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ConnexionService } from "./connexion.service";

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: ConnexionService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getToken();

    if (token) {
      // If we have a token, we set it to the header
      request = request.clone({
        setHeaders: { Authorization: `${token}` }
      });
    }

    return next.handle(request);
  }
}