import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { SHA1 } from 'crypto-js';
import { Observable, catchError, firstValueFrom } from 'rxjs';
import { ApiService } from '../api/apiService';
import { NotificationService } from '../notification/notification.service';
import { RoleName, UserDto } from './service-proxies';

@Injectable({
  providedIn: 'root'
})
export class ConnexionService extends ApiService implements CanActivate {
  constructor(protected override notificationService: NotificationService, protected httpClient: HttpClient) {
    super(notificationService, httpClient, 'connexion');
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.loginWithToken().then((res) => {
      if (res) {
        return true;
      }
      return false;
    });
  }

  async signin(username: string, firstname: string, lastname: string, password: string): Promise<boolean> {
    password = SHA1(password).toString();
    const res = await firstValueFrom(this.httpClient.post(this.path + "/signin", { username, password, firstname, lastname }));
    const data = res as ConnexionDto;
    if (data) {
      this.saveData(data);
      return true;
    }
    return false;
  }

  async login(username: string, password: string, remembered: boolean): Promise<boolean> {
    password = SHA1(password).toString();
    // eslint-disable-next-line max-len
    const res = await firstValueFrom(this.httpClient.post(this.path + "/login", { username, password, remembered }));
    const data = res as ConnexionDto;
    if (data['token']) {
      this.saveData(data);
      return true;
    }
    return false;
  }

  async loginWithToken(): Promise<boolean> {
    const token = this.getToken();
    if (token) {
      const res = await firstValueFrom(this.httpClient.post(this.path + "/login", { token }));
      const data = res as ConnexionDto;
      console.log(data);
      if (data['token']) {
        this.saveData(data);
        return true;
      }
    }
    return false;
  }

  async logout(): Promise<boolean> {
    const token = this.getToken();
    if (token) {
      const res = await firstValueFrom(this.httpClient.delete<UserDto>(this.path, { body: { token } }).pipe(catchError((err) => this.handleErrors(err, false)))) as UserDto;
      if (res) {
        this.removeData();
        return true;
      }
    }
    return false;
  }

  getUser() {
    return UserDto.fromJS(this.readUserFromStorage());
  }

  getUserRole() {
    return UserDto.fromJS(this.readUserFromStorage())?.roleName;
  }

  isUserAdmin() {
    return this.readUserFromStorage()?.roleName === RoleName.Admin;
  }

  getToken() {
    const token = this.readTokenFromStorage();
    if (!token) {
      return undefined;
    }
    return token;
  }

  tokenIsValid() {
    if (this.getToken()) {
      const expireTime = new Date(this.readExpireTimeFromStorage() || "");
      const now = new Date();
      if (expireTime > now) {
        return true;
      }
    }
    return false;
  }

  private saveData(toSave: ConnexionDto) {
    localStorage.setItem('currentUserToken', JSON.stringify(toSave.token));
    localStorage.setItem('currentUserTokenExpireTime', JSON.stringify(toSave.expire_time));
    localStorage.setItem('currentUser', JSON.stringify(toSave.user));
  }

  private removeData() {
    localStorage.removeItem('currentUserToken');
    localStorage.removeItem('currentUserTokenExpireTime');
    localStorage.removeItem('currentUser');
  }

  private readUserFromStorage() {
    const user = localStorage.getItem('currentUser');
    if (user) {
      return JSON.parse(user) as UserDto;
    }
    return undefined;
  }

  private readTokenFromStorage() {
    const token = localStorage.getItem('currentUserToken');
    if (token) {
      return JSON.parse(token) as string;
    }
    return undefined;
  }

  private readExpireTimeFromStorage() {
    const expireTime = localStorage.getItem('currentUserTokenExpireTime');
    if (expireTime) {
      return JSON.parse(expireTime) as Date;
    }
    return undefined;
  }
}


interface ConnexionDto { expire_time: string; token: string; user: { id: number; username: string; } }
