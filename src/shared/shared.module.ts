import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppSpinnerComponent } from './component/spinner/app-spinner.component';
import { MaterialModule } from './modules/material.module';

@NgModule({
  declarations: [AppSpinnerComponent],
  imports: [CommonModule, MaterialModule],
  exports: [MaterialModule, AppSpinnerComponent]
})
export class SharedModule {}
