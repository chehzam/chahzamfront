import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AppRoutingModule } from '../../app/app-routing.module';
import { AppModule } from '../../app/app.module';
import { AudioRecordingService } from '../../app/pages/importpage/audio-recording.service';
import { ConnexionService } from '../services/connexion.service';
import { SharedModule } from '../shared.module';
import { NotificationService } from './notification.service';

describe('NotificationService', () => {
  let service: NotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot({
          positionClass: 'toast-top-full-width'
        }),
        SharedModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        MatProgressBarModule,
        FormsModule,
        AppModule
      ],
      providers: [
        { provide: ToastrService, useClass: ToastrService },
        ConnexionService,
        NotificationService,
        AudioRecordingService
      ]
    });
    service = TestBed.inject(NotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
