export interface NotificationMessage{
  message: string;
  type: NotificationType;
}

export enum NotificationType{
  Success=0,
  Warning=1,
  Error=2,
  Info=3,
}
