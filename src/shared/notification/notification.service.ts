import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { NotificationMessage, NotificationType } from './notificationModel';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notificationSubject:Subject<NotificationMessage> = new Subject<NotificationMessage>();

  sendMessage(message:NotificationMessage){
    this.notificationSubject.next(message);
  }

  constructor(private toastrService:ToastrService) { 
    this.notificationSubject.subscribe((message:NotificationMessage)=>{
      switch(message.type){
      case NotificationType.Success:
        this.toastrService.success(message.message);
        break;
      case NotificationType.Warning:
        this.toastrService.warning(message.message);
        break;
      case NotificationType.Error:
        this.toastrService.error(message.message);
        break;
      case NotificationType.Info:
        this.toastrService.info(message.message);
        break;
      }
    });
  }
}
