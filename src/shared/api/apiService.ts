/* eslint-disable indent */
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, catchError, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { NotificationService } from '../notification/notification.service';
import { NotificationType } from '../notification/notificationModel';

export abstract class ApiService {
  constructor(protected notificationService: NotificationService, protected http: HttpClient, protected endpoint: string) { }

  protected get path(): string {
    return environment.apiUrl + '/' + environment.apiPrefix + '/' + environment.apiVersion + '/' + this.endpoint;
  }
  //add error handling
  getAll(httpParam: HttpParams | undefined, ignoreError: boolean): Observable<any> {
    return this.http
      .get(`${this.path}`, { params: httpParam })
      .pipe(catchError((err: HttpErrorResponse) => this.handleErrors(err, ignoreError)));
  }

  get(id: number, httpParam: HttpParams | undefined, ignoreError: boolean): Observable<any> {
    return this.http
      .get(`${this.path}/${id}`, { params: httpParam })
      .pipe(catchError((err: HttpErrorResponse) => this.handleErrors(err, ignoreError)));
  }

  update(id: number, object: object, httpParam: HttpParams | undefined, ignoreError: boolean): Observable<any> {
    return this.http
      .put(`${this.path}/${id}`, object, { params: httpParam })
      .pipe(catchError((err: HttpErrorResponse) => this.handleErrors(err, ignoreError)));
  }

  create(object: object, httpParam: HttpParams | undefined, ignoreError: boolean): Observable<any> {
    return this.http
      .post(`${this.path}`, object, { params: httpParam })
      .pipe(catchError((err: HttpErrorResponse) => this.handleErrors(err, ignoreError)));
  }

  delete(id: number, httpParam: HttpParams | undefined, ignoreError: boolean): Observable<any> {
    return this.http
      .delete(`${this.path}/${id}`, { params: httpParam })
      .pipe(catchError((err: HttpErrorResponse) => this.handleErrors(err, ignoreError)));
  }

  deleteAll(httpParam: HttpParams | undefined, ignoreError: boolean): Observable<any> {
    return this.http
      .delete(`${this.path}`, { params: httpParam })
      .pipe(catchError((err: HttpErrorResponse) => this.handleErrors(err, ignoreError)));
  }

  handleErrors(error: any, ignoreError: boolean) {
    if (!ignoreError) {
      switch (error.status) {
        case 200:
          this.notificationService.sendMessage({ message: 'success', type: NotificationType.Success });
          break;
        case 201:
          this.notificationService.sendMessage({ message: 'Created', type: NotificationType.Success });
          break;
        case 400:
          this.notificationService.sendMessage({ message: 'Bad Request', type: NotificationType.Error });
          break;
        case 401:
          this.notificationService.sendMessage({ message: 'Unauthorized', type: NotificationType.Error });
          break;
        case 403:
          this.notificationService.sendMessage({ message: 'Forbidden', type: NotificationType.Error });
          break;
        case 404:
          this.notificationService.sendMessage({ message: 'Not Found', type: NotificationType.Error });
          break;
        case 408:
          this.notificationService.sendMessage({ message: 'Request Timeout', type: NotificationType.Error });
          break;
        case 500:
          this.notificationService.sendMessage({ message: 'Internal Server Error', type: NotificationType.Error });
          break;
        case 501:
          this.notificationService.sendMessage({ message: 'Not Implemented', type: NotificationType.Error });
          break;
        case 502:
          this.notificationService.sendMessage({ message: 'Bad Gateway', type: NotificationType.Error });
          break;
        case 503:
          this.notificationService.sendMessage({ message: 'Service Unavailable', type: NotificationType.Error });
          break;
        case 543:
          this.notificationService.sendMessage({ message: 'No match found in database', type: NotificationType.Error });
          break;
        default:
          this.notificationService.sendMessage({ message: 'Unknown Error', type: NotificationType.Error });
      }
      return of(error);
    }
    return of(undefined);
  }
}
