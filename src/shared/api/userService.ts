import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { NotificationService } from "../notification/notification.service";
import { ApiService } from "./apiService";

@Injectable({
  providedIn: 'root',
})
export class UserService extends ApiService {
  constructor(protected override notificationService: NotificationService, protected httpClient: HttpClient,) {
    super(notificationService, httpClient, 'user');
  }
}