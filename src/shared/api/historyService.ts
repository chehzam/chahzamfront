import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { NotificationService } from "../notification/notification.service";
import { ConnexionService } from "../services/connexion.service";
import { ApiService } from "./apiService";

@Injectable({
  providedIn: 'root',
})
export class HistoryService extends ApiService {
  constructor(protected override notificationService: NotificationService, protected httpClient: HttpClient, protected auth:ConnexionService) {
    super(notificationService, httpClient, 'history/'+ auth.getUser().id);
  }
}
