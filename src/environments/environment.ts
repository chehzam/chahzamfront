export const environment = {
  production: true,
  apiVersion: 'v1',
  apiPrefix: 'api',
  apiUrl: 'https://chehzamwebappback.azurewebsites.net'
};
