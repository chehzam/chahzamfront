export const environment = {
  production: false,
  apiVersion: 'v1',
  apiPrefix: 'api',
  apiUrl: 'https://chehzamwebappback.azurewebsites.net'
};
