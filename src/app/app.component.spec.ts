import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NotificationService } from '../shared/notification/notification.service';
import { ConnexionService } from '../shared/services/connexion.service';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { AudioRecordingService } from './pages/importpage/audio-recording.service';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot({
          positionClass: 'toast-top-full-width'
        }),
        SharedModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        AppModule,
        RouterTestingModule
      ],
      providers: [
        { provide: ToastrService, useClass: ToastrService },
        ConnexionService,
        NotificationService,
        AudioRecordingService
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ChehZamFront'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('ChehZam');
  });
});