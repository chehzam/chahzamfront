import { Component, OnInit } from '@angular/core';
import { HistoryService } from '../../../shared/api/historyService';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { MusicDto } from '../../../shared/services/service-proxies';

@Component({
  selector: 'app-main-historic',
  templateUrl: './main-historic.component.html',
  styleUrls: ['./main-historic.component.scss']
})
export class MainHistoricComponent implements OnInit {
  history: MusicDto[] = [];
  defaultCoverPath = '/assets/stub/nosound.PNG';

  constructor(private connexionService: ConnexionService, private historyService: HistoryService) {
    this.historyService.getAll(undefined, true).subscribe((res) => {
      this.history = res;
      console.log(this.history[2]);
    });
  }
  ngOnInit(): void {}
}
