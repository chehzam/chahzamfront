import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatIconTestingModule } from '@angular/material/icon/testing';
import { MaterialModule } from '../../../shared/modules/material.module';
import { MainMusicComponent } from './main-music.component';

describe('MainMusicComponent', () => {
  let component: MainMusicComponent;
  let fixture: ComponentFixture<MainMusicComponent>;



  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MainMusicComponent],
      imports: [
        MaterialModule,
        MatIconTestingModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MainMusicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
