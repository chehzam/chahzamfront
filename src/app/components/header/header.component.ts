import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NotificationService } from '../../../shared/notification/notification.service';
import { NotificationType } from '../../../shared/notification/notificationModel';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { UserDto } from '../../../shared/services/service-proxies';
import { ErrorSwal } from '../../modal/error.modal';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public loadingSubject = new BehaviorSubject<boolean>(false);

  @Input() user: UserDto | undefined;

  constructor(private service: ConnexionService, private notif: NotificationService, private router: Router) {
    this.user = this.service.getUser();
  }

  ngOnInit(): void {}

  logout() {
    this.loadingSubject.next(true);
    this.service.logout().then((res) => {
      if (res) {
        this.router.navigate(['../login']).then((rt) => {
          if (rt) {
            this.notif.sendMessage({ message: 'Déconnexion réussie', type: NotificationType.Success });
          }
        });
      } else {
        const swal = new ErrorSwal();
        swal.open('Erreur', 'La déconnexion a échoué');
      }
      this.loadingSubject.next(false);
    });
  }
}
