import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NotificationService } from '../../../shared/notification/notification.service';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { SharedModule } from '../../../shared/shared.module';
import { AppRoutingModule } from '../../app-routing.module';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot({
          positionClass: 'toast-top-full-width'
        }),
        SharedModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
      ],
      providers: [
        { provide: ToastrService, useClass: ToastrService },
        ConnexionService,
        NotificationService
      ],
      declarations: [HeaderComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
