import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { ImportpageComponent } from './pages/importpage/importpage.component';
import { LoginpageComponent } from './pages/loginpage/loginpage.component';
import { ResultpageComponent } from './pages/resultpage/resultpage.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent
  },
  {
    path: 'result',
    component: ResultpageComponent
  },
  {
    path: ':method',
    component: LoginpageComponent
  },
  {
    path: 'search/:method',
    component: ImportpageComponent
  },
  {
    path: '**',
    component: LoginpageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
