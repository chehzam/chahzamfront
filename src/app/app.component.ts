import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConnexionService } from '../shared/services/connexion.service';
import { UserDto } from '../shared/services/service-proxies';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnChanges {
  title = 'ChehZam';
  url = '';
  constructor(private authService: ConnexionService, private router: Router) {
    if (!this.authService.tokenIsValid()) {
      console.log('not logged in');
      this.router.navigate(['../login']);
    }
  }

  ngOnInit(): void {
    this.url = this.router.url;
  }

  ngOnChanges() {
    if (this.router.url !== this.url) {
      this.url = this.router.url;
      console.log('changed');
    }
  }

  getUser(): UserDto | undefined {
    const user = this.authService.getUser();
    return user?.id ? user : new UserDto();
  }
}
