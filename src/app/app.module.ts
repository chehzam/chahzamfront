import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogConfig, MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AuthInterceptorService } from '../shared/services/authInterceptor';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MainHistoricComponent } from './components/main-historic/main-historic.component';
import { MainMusicComponent } from './components/main-music/main-music.component';
import { AddMusicDialogComponent } from './modal/add-music-dialog/add-music-dialog.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { AudioRecordingService } from './pages/importpage/audio-recording.service';
import { ImportpageComponent } from './pages/importpage/importpage.component';
import { LoginpageComponent } from './pages/loginpage/loginpage.component';
import { MusicTrigger } from './pages/musictrigger';
import { ResultpageComponent } from './pages/resultpage/resultpage.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainMusicComponent,
    MainHistoricComponent,
    FooterComponent,
    HomepageComponent,
    LoginpageComponent,
    ImportpageComponent,
    ResultpageComponent,
    AddMusicDialogComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    SharedModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatDialogModule,
    OverlayModule,
    BrowserAnimationsModule
  ],
  providers: [
    MusicTrigger,
    AudioRecordingService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    MatDialogConfig
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
