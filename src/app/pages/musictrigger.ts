import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MusicDto } from '../../shared/services/service-proxies';

@Injectable({ providedIn: 'root' })
export class MusicTrigger {
  private music: MusicDto | undefined;
  private subject;

  constructor() {
    this.subject = new BehaviorSubject<MusicDto | undefined>(this.music);
  }
  sendMessage(value: MusicDto) {
    this.music = value;
    this.subject.next(value);
  }

  clearMessages() {
    this.music = undefined;
    this.subject.next(undefined);
  }

  getMessage(): Observable<MusicDto | undefined> {
    return this.subject.asObservable();
  }
}
