import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import moment from 'moment';
import { Observable } from 'rxjs';
import { HistoryService } from '../../../shared/api/historyService';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { MusicDto } from '../../../shared/services/service-proxies';
import { MusicTrigger } from '../musictrigger';

@Component({
  selector: 'app-resultpage',
  templateUrl: './resultpage.component.html',
  styleUrls: ['./resultpage.component.scss']
})
export class ResultpageComponent implements OnInit, OnDestroy {
  music: MusicDto | undefined;
  observable: Observable<MusicDto | undefined>;

  constructor(
    private service: ConnexionService,
    private router: Router,
    private trigger: MusicTrigger,
    private historyService: HistoryService,
    private connexionService: ConnexionService
  ) {
    this.observable = this.trigger.getMessage();
    this.observable.subscribe((res) => this.musicChanged(res!));
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.trigger.clearMessages();
  }

  displayArtists(): string {
    let value = '';
    let i = 0;
    const size = this.music?.featuring.length;
    value += this.music?.artist.name + ' ';
    this.music?.featuring.forEach((auth) => {
      if (i === size! - 1) {
        value = value + auth.name;
      } else value = value + auth.name + ', ';
      i = i + 1;
    });
    return value;
  }

  getSpotifyHref() {
    return `https://open.spotify.com/search/${this.music?.title}, ${this.music?.artist.name}`;
  }

  getAppleMusicHref() {
    let value = 'https://www.apple.com/fr/search/so-la-lune-fin-heureuse';
    const suffix = '?src=globalnav';
    const title = this.music?.title;
    value += title?.replaceAll(' ', '-');
    value += '-';
    const author = this.music?.artist ? this.music?.artist.name : '';
    value += author?.replaceAll(' ', '-');
    value += suffix;
    return value;
  }

  musicChanged(dto: MusicDto) {
    if (dto && dto.id_music !== this.music?.id_music) {
      this.music = dto;
      this.historyService
        .create(
          { id: 0, id_music: this.music.id_music, id_user: this.connexionService.getUser().id, history_date: moment() },
          undefined,
          false
        )
        .subscribe(() => console.log('Music added to history'));
    }
  }
}
