import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NotificationService } from '../../../shared/notification/notification.service';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { SharedModule } from '../../../shared/shared.module';
import { AppRoutingModule } from '../../app-routing.module';
import { AppModule } from '../../app.module';
import { AudioRecordingService } from '../importpage/audio-recording.service';
import { ResultpageComponent } from './resultpage.component';

describe('ResultpageComponent', () => {
  let component: ResultpageComponent;
  let fixture: ComponentFixture<ResultpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot({
          positionClass: 'toast-top-full-width'
        }),
        SharedModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        AppModule
      ],
      providers: [
        { provide: ToastrService, useClass: ToastrService },
        ConnexionService,
        NotificationService,
        AudioRecordingService
      ],
      declarations: [ResultpageComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ResultpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
