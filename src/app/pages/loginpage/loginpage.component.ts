import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnexionService } from '../../../shared/services/connexion.service';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.scss']
})
export class LoginpageComponent {
  loginForm: FormGroup = this.fb.group({});
  registerForm: FormGroup = this.fb.group({});
  method = '';
  loginImgSrc = 'assets/sign_in_text.PNG';
  registerImgSrc = 'assets/register_text.PNG';

  constructor(private authService: ConnexionService, private router: Router, private fb: FormBuilder, private route: ActivatedRoute) {
    if (this.authService.getUser().id !== undefined) {
      this.router.navigate(['../']);
    }
    this.method = String(this.route.snapshot.paramMap.get('method'));
    if (this.method !== 'login' && this.method !== 'register') {
      this.router.navigate(['../login']);
      this.method = 'login';
    }
    this.loginForm = this.fb.group({
      login: ['', [Validators.minLength(2), Validators.required]],
      password: ['', [Validators.minLength(2), Validators.required]]
    });
    this.registerForm = this.fb.group({
      login: ['', [Validators.minLength(2), Validators.required]],
      firstname: ['', [Validators.minLength(2), Validators.required]],
      lastname: ['', [Validators.minLength(2), Validators.required]],
      password: ['', [Validators.minLength(2), Validators.required]],
      repassword: ['', [Validators.minLength(2)]]
    });
  }

  loginButtonClick() {
    if (this.method === 'login') {
      this.login();
    } else this.register();
  }

  register() {
    if (this.registerForm.value.password !== this.registerForm.value.repassword) {
      //Add notification
      return;
    }
    //const checkboxInput = (document.getElementById('checkboxInput') as HTMLInputElement).checked;
    this.authService
      .signin(
        this.registerForm.value.login,
        this.registerForm.value.firstname,
        this.registerForm.value.lastname,
        this.registerForm.value.password
      )
      .then((res) => {
        if (res) {
          console.log('logged in');
          //navigate to homepage
          this.router.navigate(['/']);
        } else {
          console.log('not logged in');
          //navigate to login page or show error message
          // TODO
          this.router.navigate(['/login']);
        }
      });
  }

  login() {
    //const checkboxInput = (document.getElementById('checkboxInput') as HTMLInputElement).checked;
    this.authService.login(this.loginForm.value.login, this.loginForm.value.password, false).then((res) => {
      if (res) {
        console.log('logged in');
        //navigate to homepage
        this.router.navigate(['/']);
      } else {
        console.log('not logged in');
        //navigate to login page or show error message
        // TODO
        this.router.navigate(['/login']);
      }
    });
  }

  changeMethod(event: MatTabChangeEvent) {
    if (event.index === 1) {
      this.method = 'register';
    } else this.method = 'login';
  }
}
