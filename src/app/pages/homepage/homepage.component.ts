import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AddMusicService } from '../../../shared/api/addMusicService';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { RoleName } from '../../../shared/services/service-proxies';
import { AddMusicDialogComponent } from '../../modal/add-music-dialog/add-music-dialog.component';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  RoleName = RoleName;
  role: RoleName;

  constructor(private service: ConnexionService, private router: Router, private dialog: MatDialog, private addService: AddMusicService) {
    if (this.service.getUser().id === undefined) {
      this.router.navigate(['../login']);
    }
    this.role = this.service.getUserRole() || RoleName.None;
    if (this.role === RoleName.None) {
      this.router.navigate(['../login']);
    }
  }

  ngOnInit(): void {}

  openAdminDialog() {
    const ref = this.dialog.open(AddMusicDialogComponent, {
      minWidth: '40%'
    });
    ref.afterClosed().subscribe((res) => {
      if (res) {
        this.addService.create({ data: res }, undefined, true).subscribe(() => {
          console.log('music added');
        });
      }
    });
  }
}
