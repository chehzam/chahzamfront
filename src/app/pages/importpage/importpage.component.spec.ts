import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NotificationService } from '../../../shared/notification/notification.service';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { SharedModule } from '../../../shared/shared.module';
import { AppRoutingModule } from '../../app-routing.module';
import { AppModule } from '../../app.module';
import { AudioRecordingService } from './audio-recording.service';
import { ImportpageComponent } from './importpage.component';

describe('ImportpageComponent', () => {
  let component: ImportpageComponent;
  let fixture: ComponentFixture<ImportpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot({
          positionClass: 'toast-top-full-width'
        }),
        SharedModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        AppModule
      ],
      providers: [
        { provide: ToastrService, useClass: ToastrService },
        ConnexionService,
        NotificationService,
        AudioRecordingService
      ],
      declarations: [ImportpageComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ImportpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
