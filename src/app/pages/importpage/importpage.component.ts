import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { MusicService } from '../../../shared/api/musicService';
import { ConnexionService } from '../../../shared/services/connexion.service';
import { MusicDto } from '../../../shared/services/service-proxies';
import { MusicTrigger } from '../musictrigger';
import { AudioRecordingService, RecordedAudioOutput } from './audio-recording.service';

@Component({
  selector: 'app-importpage',
  templateUrl: './importpage.component.html',
  styleUrls: ['./importpage.component.scss']
})
export class ImportpageComponent implements OnInit, OnDestroy {
  audioURL = '';
  audio = new Audio();
  recognized: MusicDto = new MusicDto();

  recordedTime = '';
  blobUrl: SafeUrl = '';
  output: RecordedAudioOutput = { blob: new Blob(), title: '' };

  fileReader: FileReader;
  fileBase64 = '';

  public recordingSubject = new BehaviorSubject<boolean>(false);

  private sampleRate = 44800;
  private recordedSubject = new BehaviorSubject<boolean>(false);
  public recorded$ = this.recordedSubject.asObservable();

  public loadingSubject = new BehaviorSubject<boolean>(false);
  _importId = 0;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private audioRecordingService: AudioRecordingService,
    private sanitizer: DomSanitizer,
    private service: ConnexionService,
    private router: Router,
    private musicService: MusicService,
    private trigger: MusicTrigger
  ) {
    this._importId = String(this.route.snapshot.paramMap.get('method')) === 'mic' ? 0 : 1;
    this.fileReader = new FileReader();
  }

  ngOnInit() {
    if (!this.service.getUser()) {
      this.router.navigate(['../login']);
    }
    this.trigger.clearMessages();
    this.audioRecordingService.recordingFailed().subscribe(() => this.recordedSubject.next(false));
    this.audioRecordingService.getRecordedTime().subscribe((time) => (this.recordedTime = time));
    this.audioRecordingService.getRecordedBlob().subscribe((data) => {
      this.output = data;
      this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob));
    });
    this.fileReader.onload = () => {
      const srcData = this.fileReader.result;
      this.fileBase64 = srcData?.toString()!;
    };
  }

  processFile(fileInput: any) {
    if (fileInput.files[0]) {
      const file: File = fileInput.files[0];
      const pattern = /.wav/;
      if (!file.type.match(pattern)) {
        alert('Invalid format');
        return;
      }
      this.convertFileToBase64(file);
      this.loadingSubject.next(true);
      this.musicService.create({ data: this.fileBase64, sampleRate: this.sampleRate }, undefined, false).subscribe((value) => {
        if (value && value instanceof MusicDto) {
          this.recognized = value;
          if (this.recognized) {
            this.trigger.sendMessage(this.recognized);
            this.router.navigate(['../result']);
          }
        }
        this.loadingSubject.next(false);
      });
    }
  }

  convertFileToBase64(file: Blob) {
    this.fileReader.readAsDataURL(file);
  }

  sendMessage(): void {
    this.trigger.sendMessage(this.recognized);
  }

  clearMessages(): void {
    this.trigger.clearMessages();
  }

  startRecording() {
    this.recordingSubject.next(true);
    this.audioRecordingService.startRecording();
  }

  abortRecording() {
    if (!this.recordingSubject.value) {
      this.recordedSubject.next(false);
      this.audioRecordingService.abortRecording();
    }
  }

  stopRecording() {
    this.audioRecordingService.stopRecording();
    this.recordingSubject.next(false);
  }

  clearRecordedData() {
    this.blobUrl = '';
  }

  ngOnDestroy(): void {
    this.abortRecording();
  }

  download(): void {
    const url = window.URL.createObjectURL(this.output.blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = this.output.title;
    link.click();
  }

  recButtonClick() {
    if (this._importId === 0) {
      if (!this.recordingSubject.value) {
        this.startRecording();
      } else this.stopRecording();
    }
  }

  upload() {}

  recognize() {
    this.loadingSubject.next(true);
    const audioElement = document.getElementById('audio-element') as HTMLAudioElement;
    const audioUrl = audioElement.src;
    fetch(audioUrl)
      .then((response) => response.blob())
      .then((blob) => {
        const fileReader = new FileReader();
        fileReader.onload = () => {
          const base64Audio = fileReader.result as string;
          this.musicService.create({ data: base64Audio, sampleRate: this.sampleRate }, undefined, false).subscribe((value) => {
            console.log(typeof value);
            if (value && !(value instanceof HttpErrorResponse)) {
              this.recognized = value;
              if (this.recognized) {
                this.trigger.sendMessage(this.recognized);
                this.router.navigate(['../result']);
              }
            }
            this.loadingSubject.next(false);
          });
        };
        fileReader.readAsDataURL(blob);
      });
  }
}
