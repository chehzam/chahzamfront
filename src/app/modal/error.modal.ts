import Swal, { SweetAlertResult } from 'sweetalert2';

export class ErrorSwal {
  constructor() {}

  open(title: string, message: string): Promise<SweetAlertResult> {
    return Swal.fire({
      title,
      text: message,
      icon: 'warning',
      showCancelButton: false,
      confirmButtonText: 'OK'
    });
  }
}
