import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MaterialModule } from '../../../shared/modules/material.module';
import { AddMusicDialogComponent } from './add-music-dialog.component';

describe('AddMusicDialogComponent', () => {
  let component: AddMusicDialogComponent;
  let fixture: ComponentFixture<AddMusicDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddMusicDialogComponent],
      imports: [
        MaterialModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        }
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AddMusicDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
