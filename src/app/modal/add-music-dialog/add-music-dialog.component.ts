import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-music-dialog',
  templateUrl: './add-music-dialog.component.html',
  styleUrls: ['./add-music-dialog.component.scss']
})
export class AddMusicDialogComponent implements OnInit {
  fileBase64 = '';
  fileReader: FileReader;
  form: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddMusicDialogComponent>, private fb: FormBuilder) {
    this.fileReader = new FileReader();
    this.form = this.fb.group({
      audio: ['', Validators.required]
    });
  }
  ngOnInit(): void {
    this.fileReader.onload = () => {
      const srcData = this.fileReader.result;
      this.fileBase64 = srcData?.toString()!;
    };
  }

  processFile(fileInput: any) {
    if (fileInput.files[0]) {
      const file: File = fileInput.files[0];
      const pattern = /.flac/;
      if (!file.type.match(pattern)) {
        alert('Invalid format');
        return;
      }
      this.convertFileToBase64(file);
    }
  }

  convertFileToBase64(file: Blob) {
    this.fileReader.readAsDataURL(file);
  }

  submit() {
    if (this.fileBase64 && this.fileBase64 !== '') {
      this.dialogRef.close(this.fileBase64);
    }
  }
}
