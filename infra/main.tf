resource "random_integer" "ri" {
  min = 10000
  max = 99999
}

resource "azurerm_resource_group" "rg" {
  name     = "chehzam"
  location = "France Central"
}

resource "azurerm_service_plan" "sp" {
  name                = "sp"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "webApp" {
  name                = "frontShehzamFront"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_service_plan.sp.location
  service_plan_id     = azurerm_service_plan.sp.id

  site_config {
    application_stack {
      docker_image_name = "coucou619/chehzam:frontend"
      docker_registry_url = "https://index.docker.io"
      docker_registry_username = "coucou619"
      docker_registry_password = "coucoucou"
    }
  }

  app_settings = {
    SCM_DO_BUILD_DURING_DEPLOYMENT = "true"
  }
}